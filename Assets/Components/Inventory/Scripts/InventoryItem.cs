using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{


    public bool _isEquippable;
    private GameObject _itemPendingEqiping;
    public bool _isEquipped; // когда уже в слотах 
    public bool _isSelected; //когда его будет видно в руках у игрока


    private void Start()
    {
       
    }

    private void Update()
    {
        if (_isSelected)
        {
            gameObject.GetComponent<DragDrop>().enabled = false;
        }
        else
        {
            gameObject.GetComponent<DragDrop>().enabled = true;
        }
    }




    // Triggered when the mouse enters into the area of the item that has this script.
    public void OnPointerEnter(PointerEventData eventData)
    {
        
    }

    // Triggered when the mouse exits the area of the item that has this script.
    public void OnPointerExit(PointerEventData eventData)
    {
       
    }

    // Triggered when the mouse is clicked over the item that has this script.
    public void OnPointerDown(PointerEventData eventData)
    {
        if (_isEquippable && !_isEquipped && !PanelManager.Instance.CheckIfFull()) 
        {
            PanelManager.Instance.AddToQuickSlots(gameObject);
            _isEquipped = true;
        }
    }

    // Triggered when the mouse button is released over the item that has this script.
    public void OnPointerUp(PointerEventData eventData)
    {

    }

    private void consumingFunction(float healthEffect, float caloriesEffect, float hydrationEffect)
    {


    }


    private static void healthEffectCalculation(float healthEffect)
    {

    }


    private static void caloriesEffectCalculation(float caloriesEffect)
    {

    }


    private static void hydrationEffectCalculation(float hydrationEffect)
    {

    }


}
