using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BluePrint 
{
    public string _itemName;

    public string _req1;

    public string _req2;
    //public string _req3;

    public int _req1_amount;
    public int _req2_amount;
    //public int _req3_amount;

    public int _total_req_amount;

    public BluePrint(string name, int reqNum, string R1, int R1num, string R2, int R2num)
    {
     
        _itemName = name;
        _total_req_amount = reqNum;
        _req1 = R1;
        _req1_amount = R1num;
        _req2 = R2;
        _req2_amount = R2num;
    }
    //public BluePrintGun(string name, int reqNum, string R1, int R1num, string R2, int R2num, string R3, int R3num)
    //{

    //    _itemName = name;
    //    _total_req_amount = reqNum;
    //    _req1 = R1;
    //    _req1_amount = R1num;
    //    _req2 = R2;
    //    _req2_amount = R2num;
    //    _req3 = R3;
    //    _req3_amount = R3num;
    //}
}
