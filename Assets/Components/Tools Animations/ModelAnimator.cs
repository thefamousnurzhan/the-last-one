using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Animator))]
public class ModelAnimator : MonoBehaviour
{
    public Animator _animator;
    public GameObject _gameObject;
    private void Start()
    {
        _animator = GetComponent<Animator>();
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && InventoryManager.Instance._isOpen == false && CraftingManager.Instance._isOpen == false)
        {
            //Debug.Log(_gameObject.name);
            if(_gameObject.name == "Axe_Model(Clone)")
            {
                _animator.SetTrigger("Swing");
                StartCoroutine(SwingSoundDelay());
            }
            else if(_gameObject.name == "Gun_Model(Clone)")
            {
                _animator.SetTrigger("Pistol");
                StartCoroutine(PistolSoundDelay());
            }
            else if(_gameObject.name == "Sword_Model(Clone)")
            {
                _animator.SetTrigger("Swing");
                StartCoroutine(SwordSoundDelay());        
            }
        }
        
    }
    private IEnumerator SwingSoundDelay()
    {
        yield return new WaitForSeconds(0.5f);
        SoundManager.Instance.PlaySound(SoundManager.Instance._axeSwing);
    }
    private IEnumerator PistolSoundDelay()
    {
        yield return new WaitForSeconds(0.6f);
        SoundManager.Instance.PlaySound(SoundManager.Instance._pistol);
    }
    private IEnumerator SwordSoundDelay()
    {
        yield return new WaitForSeconds(0.4f);
        SoundManager.Instance.PlaySound(SoundManager.Instance._sword);
    }

}
