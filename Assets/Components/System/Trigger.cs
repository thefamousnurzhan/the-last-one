using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Trigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "Player")
        {
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene("Finish_Game");
        }
    }
}
