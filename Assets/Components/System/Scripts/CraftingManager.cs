using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class CraftingManager : MonoBehaviour
{
    public static CraftingManager Instance { get; set; }

    public GameObject _craftingScreenUI;
    public GameObject _ToolsScreenUI;
    public GameObject _GunsScreenUI;

    public List<string> InventoryITEMS = new List<string>();        // cоздали лист который примет все что есть в инвентаре

    Button _toolsButton;
    Button _gunsButton;


    Button _axe_craftButton;
    Button _sword_craftButton;
    Button _gun_craftButton;
    // текст для крафта;

    TextMeshProUGUI _requirement1_axe, _requirement2_axe, _req1_sword, _req2_sword, _req1_gun, _req2_gun,_req3_gun;
    // is open or not flag;
    public bool _isOpen;

    // BLUEPrint
    public BluePrint Axe_BluePrint = new BluePrint("Axe", 2, "Rock", 1, "Stick", 1);
    public BluePrint Sword_bluePrint = new BluePrint("Sword", 3, "Rock", 2, "Stick", 1);
    public BluePrintGun Gun_bluePrint = new BluePrintGun("Gun", 4, "Rock", 2, "Stick", 1, "Powder", 1);


    private void Awake()

    {
        if(Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        // _toolsScreenUI
        _isOpen = false;
        _toolsButton = _craftingScreenUI.transform.Find("ToolsBTN").GetComponent<Button>();
        _toolsButton.onClick.AddListener(delegate { OpenToolsScreen(); });

        // AXE
        _requirement1_axe = _ToolsScreenUI.transform.Find("Axe").transform.Find("req1").GetComponent<TextMeshProUGUI>();
        _requirement2_axe = _ToolsScreenUI.transform.Find("Axe").transform.Find("req2").GetComponent<TextMeshProUGUI>();
        _axe_craftButton = _ToolsScreenUI.transform.Find("Axe").transform.Find("CraftAXE").GetComponent<Button>();
        _axe_craftButton.onClick.AddListener(delegate { CraftItem(Axe_BluePrint); });
        //

        // Sword
        _req1_sword = _ToolsScreenUI.transform.Find("Sword").transform.Find("req1").GetComponent<TextMeshProUGUI>();
        _req2_sword = _ToolsScreenUI.transform.Find("Sword").transform.Find("req2").GetComponent<TextMeshProUGUI>();
        _sword_craftButton = _ToolsScreenUI.transform.Find("Sword").transform.Find("CraftSword").GetComponent<Button>();
        _sword_craftButton.onClick.AddListener(delegate { CraftItem(Sword_bluePrint); });

        //====================================================================================================//

        // _gunsScreenUI;
        _gunsButton = _craftingScreenUI.transform.Find("GunsBTN").GetComponent<Button>();
        _gunsButton.onClick.AddListener(delegate { OpenGunsScreen(); });

        //Gun
        _req1_gun = _GunsScreenUI.transform.Find("Gun").transform.Find("req1").GetComponent<TextMeshProUGUI>();
        _req2_gun= _GunsScreenUI.transform.Find("Gun").transform.Find("req2").GetComponent<TextMeshProUGUI>();
        _req3_gun = _GunsScreenUI.transform.Find("Gun").transform.Find("req3").GetComponent<TextMeshProUGUI>();
        _gun_craftButton = _GunsScreenUI.transform.Find("Gun").transform.Find("CraftGun").GetComponent<Button>();
        _gun_craftButton.onClick.AddListener(delegate { CraftItemGun(Gun_bluePrint); });
    }



    private void CraftItem(BluePrint bluePrint)
    {
        SoundManager.Instance.PlaySound(SoundManager.Instance._crafting);
        // добавить что то в инвентарь
        InventoryManager.Instance.AddToInventory(bluePrint._itemName);
        
        // удалить ресурсы из инвентаря


        if (bluePrint._total_req_amount == 1)
        {
            InventoryManager.Instance.RemoveItem(bluePrint._req1, bluePrint._req1_amount);
        }
        else if(bluePrint._total_req_amount == 2)
        {
            InventoryManager.Instance.RemoveItem(bluePrint._req1, bluePrint._req1_amount);
            InventoryManager.Instance.RemoveItem(bluePrint._req2, bluePrint._req2_amount);
        }
        else if (bluePrint._total_req_amount == 3)
        {
            InventoryManager.Instance.RemoveItem(bluePrint._req1, bluePrint._req1_amount);
            InventoryManager.Instance.RemoveItem(bluePrint._req1, bluePrint._req1_amount);
            InventoryManager.Instance.RemoveItem(bluePrint._req2, bluePrint._req2_amount);
        }

        // обновить листы слотов и ресурсов
        StartCoroutine(calculate());

        //
        RefreshNeededItems();

    }


    private void CraftItemGun(BluePrintGun gun_bluePrint)
    {
        SoundManager.Instance.PlaySound(SoundManager.Instance._crafting);
        InventoryManager.Instance.AddToInventory(gun_bluePrint._itemName);

        if (gun_bluePrint._total_req_amount == 4)
        {
            InventoryManager.Instance.RemoveItem(gun_bluePrint._req1, gun_bluePrint._req1_amount);
            InventoryManager.Instance.RemoveItem(gun_bluePrint._req1, gun_bluePrint._req1_amount);
            InventoryManager.Instance.RemoveItem(gun_bluePrint._req2, gun_bluePrint._req2_amount);
            InventoryManager.Instance.RemoveItem(gun_bluePrint._req3, gun_bluePrint._req3_amount);
        }

        StartCoroutine(calculate());

       
    }



    public IEnumerator calculate()
    {
        yield return new WaitForSeconds(2f);
        InventoryManager.Instance.ReconstructLists();
        RefreshNeededItems();
    }





    private void OpenToolsScreen()
    {
        _craftingScreenUI.SetActive(false);
        _ToolsScreenUI.SetActive(true);
    }
    private void OpenGunsScreen()
    {
        _craftingScreenUI.SetActive(false);
        _GunsScreenUI.SetActive(true);
    }



    private void Update()
        {
            RefreshNeededItems();

            if (Input.GetKeyDown(KeyCode.Tab) && !_isOpen)
            {
                _craftingScreenUI.SetActive(true);
                _isOpen = true;
                Cursor.lockState = CursorLockMode.None;
            }
            else if (Input.GetKeyDown(KeyCode.Tab) && _isOpen)
            {
                _craftingScreenUI.SetActive(false);
                _ToolsScreenUI.SetActive(false);
                _GunsScreenUI.SetActive(false);
                _isOpen = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }

    private void RefreshNeededItems()
    {
        int rock_count = 0;
        int stick_count = 0;
        int powder_count = 0;

        InventoryITEMS = InventoryManager.Instance._itemList;
        foreach (string ItemName in InventoryITEMS)
        {
            switch (ItemName)
            {
                case "Rock":
                    rock_count += 1;
                    break;
                case "Stick":
                    stick_count += 1;
                    break;
                case "Powder":
                    powder_count += 1;
                    break;
            }

        }

        // AXe Crafting
        _requirement1_axe.text = "1 Rock[" + rock_count + "]";
        _requirement2_axe.text = "1 Stick[" + stick_count + "]";

        if (rock_count >= 1 && stick_count >= 1)
        {
            _axe_craftButton.gameObject.SetActive(true);
        }
        else
        {
            _axe_craftButton.gameObject.SetActive(false);
        }



        // Sword crafting

        _req1_sword.text = "2 Rocks[" + rock_count + "]";
        _req2_sword.text = "1 Stick[" + stick_count + "]";

        if (rock_count >= 2 && stick_count >= 1)
        {
            _sword_craftButton.gameObject.SetActive(true);
        }
        else
        {
            _sword_craftButton.gameObject.SetActive(false);
        }


        // Guns crafting

        _req1_gun.text = "2 Rocks[" + rock_count + "]";
        _req2_gun.text = "1 Stick[" + stick_count + "]";
        _req3_gun.text = "1 Powder[" + powder_count + "]";


        if(rock_count >= 2 && stick_count >= 1 && powder_count >= 1)
        {
            _gun_craftButton.gameObject.SetActive(true);
        }
        else
        {
            _gun_craftButton.gameObject.SetActive(false);
        }
    }





}
