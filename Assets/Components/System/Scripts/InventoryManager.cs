using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections;

public class InventoryManager : MonoBehaviour
{

    public static InventoryManager Instance { get; set; }

    public List<GameObject> _slotList = new List<GameObject>(); // лист слотов;
    public List<string> _itemList = new List<string>();         // лист имен вещей;

    private GameObject _itemToADD;
    private GameObject _whatSlotToEnter;

    public bool _isFull;
    public bool _isOpen;

    public GameObject inventoryScreenUI;

    //popup pickUp
    public GameObject _pickup;
    public TextMeshProUGUI _namePickup;
    public Image _imagePickup;

    //Inventory is full;
    public GameObject _InvetoryFullAlert;
    public TextMeshProUGUI _FullInventoryTEXT;
    public Image _AlertImage;
    


    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }


    void Start()
    {
        _isOpen = false;
        _isFull = false;
        MakeSlotListFull();                                     // заполняем лист перед игрой;
    }

    private void MakeSlotListFull()
    {
        foreach(Transform slot in inventoryScreenUI.transform)      // тэг для слотов; что бы заполнить их;
        {
            if (slot.CompareTag("Slot"))
            {
                _slotList.Add(slot.gameObject);
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab) && !_isOpen)
        {
            inventoryScreenUI.SetActive(true);
            _isOpen = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else if (Input.GetKeyDown(KeyCode.Tab) && _isOpen)
        {
            inventoryScreenUI.SetActive(false);
            _isOpen = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

    }


    public void AddToInventory(string ItemName)
    {
            _whatSlotToEnter = FindEmptySlot();
            _itemToADD = Instantiate(Resources.Load<GameObject>(ItemName), _whatSlotToEnter.transform.position, _whatSlotToEnter.transform.rotation);
            _itemToADD.transform.SetParent(_whatSlotToEnter.transform);

            _itemList.Add(ItemName);
            // попап меню
            Popup(ItemName, _itemToADD.GetComponent<Image>().sprite);
            
    }


    private void Popup(string ItemName, Sprite ItemImage)
    {
        _pickup.SetActive(true);
        _namePickup.text = ItemName + " is picked";
        _imagePickup.sprite = ItemImage;
        StartCoroutine(DelayPopup());
    }


    public IEnumerator DelayPopup()
    {
        yield return new WaitForSeconds(2f);
        _pickup.SetActive(false);
    }



    private GameObject FindEmptySlot()
    {
        foreach(GameObject slot in _slotList)
        {
            if (slot.transform.childCount == 0)
            {
                return slot;
            }
        }
        return new GameObject();
    }


    public bool CheckInventoryFULL()
    {
        int counter = 0;
        foreach(GameObject slot in _slotList)
        {
            if(slot.transform.childCount > 0)
            {
                counter += 1;
            }
        }
        if (counter == 6)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    // алгоритм для убирания вещей из инвентаря:    
    public void RemoveItem(string nameToRemove, int amountToRemove)
    {

        int counter = amountToRemove;
        for (int i = _slotList.Count - 1; i >= 0; i--)
        {
            if (_slotList[i].transform.childCount > 0) // если есть что то внутри слота:
            {
                if (_slotList[i].transform.GetChild(0).name == nameToRemove + "(Clone)" && counter != 0)
                {
                    Destroy(_slotList[i].transform.GetChild(0).gameObject);
                    counter -= 1;
                }
            }
        }

    }






    public void ReconstructLists()
    {
        _itemList.Clear();

        foreach (GameObject slot in _slotList)
        {
            if (slot.transform.childCount > 0) // елси что то есть внутри слота
            {
                string name = slot.transform.GetChild(0).name;

                string str2 = "(Clone)";

                string result = name.Replace(str2, "");


                _itemList.Add(result);
            }
        }
    }
}