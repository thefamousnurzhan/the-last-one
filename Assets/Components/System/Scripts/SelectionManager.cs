using UnityEngine;
using TMPro;


public class SelectionManager : MonoBehaviour

{

        public GameObject interaction_Info_UI;
        TextMeshProUGUI interaction_text;
        // получаем компонент текста для дальнейшего скриптинга
        private void Start()
        {
            interaction_text = interaction_Info_UI.GetComponent<TextMeshProUGUI>();
        }

        void Update()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var selectionTransform = hit.transform;
                var Interactable = selectionTransform.GetComponent<InteractableObject>();
                //если игрок в радусе то он может увидеть
                if (Interactable && Interactable.PlayerInRange)
                {
                    interaction_text.text = selectionTransform.GetComponent<InteractableObject>().GetItemName();
                    interaction_Info_UI.SetActive(true);
                }
                else
                {
                    interaction_Info_UI.SetActive(false);
                }

            }
        }
}
