using UnityEngine;
using System.Collections;

public class InteractableObject : MonoBehaviour
{
    public bool PlayerInRange; 
    public string ItemName;

    public string GetItemName()
    {
        return ItemName;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerInRange = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerInRange = false;
        }
    }



    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && PlayerInRange)
        {
            if (!InventoryManager.Instance.CheckInventoryFULL())
            {
                InventoryManager.Instance.AddToInventory(ItemName);
                SoundManager.Instance.PlaySound(SoundManager.Instance._pickUpSound);
                Destroy(gameObject);
            }
            else
            {
                InventoryManager.Instance._InvetoryFullAlert.SetActive(true);
                InventoryManager.Instance._FullInventoryTEXT.text = "Inventory is full!";
                InventoryManager.Instance._FullInventoryTEXT.color = Color.red;
                StartCoroutine(Delay());
            }
        }
    }
    private IEnumerator Delay()
    {
        yield return new WaitForSeconds(2f);
        InventoryManager.Instance._InvetoryFullAlert.SetActive(false);
    }
    
}