using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestManager : MonoBehaviour
{
    public static QuestManager Instance { get; private set; }
    public GameObject questScreenUI;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        DisplayQuestScreen();
    }

    private void DisplayQuestScreen()
    {
        questScreenUI.SetActive(true);
        StartCoroutine(HideQuestScreen());
    }

    private IEnumerator HideQuestScreen()
    {
        yield return new WaitForSeconds(4f);
        questScreenUI.SetActive(false);
    }
}

