using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelManager : MonoBehaviour
{
    public static PanelManager Instance { get; set; }

    // UI
    public GameObject quickSlotsPanel;
    public RectTransform _Panel1_change;
    public RectTransform _Panel2_change;
    public RectTransform _Panel3_change;

    public List<GameObject> quickSlotsList = new List<GameObject>();
    public List<string> itemList = new List<string>();

    public GameObject _selectedItem;

    public GameObject _SelectedItemModel;



    //public GameObject selectedItem;

    // toolholder
    public GameObject _toolHolder;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }


    private void Start()
    {
        PopulateSlotList();
    }



    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ChangePanelSlot(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ChangePanelSlot(2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ChangePanelSlot(3);
        }
    }

    private void ChangePanelSlot(int number)
    {
        float _postionOnSelectedY = -170;
        if (checkPanelFull(number) == true)
        {
            if(_selectedItem != null)
            {
                _selectedItem.gameObject.GetComponent<InventoryItem>()._isSelected = false;
            }

            _selectedItem = getSelectedItem(number);
            _selectedItem.GetComponent<InventoryItem>()._isSelected = true; 
            
            if (number == 1)
            {
                if(_SelectedItemModel != null)
                {
                    DestroyImmediate(_SelectedItemModel.gameObject);
                    _SelectedItemModel = null;
                }
                SetEquippedModel(_selectedItem);

                Vector2 currentPosition = _Panel1_change.anchoredPosition;
                currentPosition.y = _postionOnSelectedY;
                _Panel1_change.anchoredPosition = currentPosition;

                 /////ad//a/df/af/ad/fa/sf//asdf/a/fa/s
                 // делаем дефолтную позицию для других 
                ResetPositionOfPanel(_Panel2_change, -10.6f);
                ResetPositionOfPanel(_Panel3_change, 57f);
            }
            else if (number == 2)
            {
                if (_SelectedItemModel != null)
                {
                    DestroyImmediate(_SelectedItemModel.gameObject);
                    _SelectedItemModel = null;
                }
                SetEquippedModel(_selectedItem);
                Vector2 currentPosition = _Panel2_change.anchoredPosition;
                currentPosition.y = _postionOnSelectedY;
                _Panel2_change.anchoredPosition = currentPosition;
                
                //=================//
                ResetPositionOfPanel(_Panel3_change, 57f);
                ResetPositionOfPanel(_Panel1_change, -78.6f);
            }
            else if (number == 3)
            {
                if (_SelectedItemModel != null)
                {
                    DestroyImmediate(_SelectedItemModel.gameObject);
                    _SelectedItemModel = null;
                }
                SetEquippedModel(_selectedItem);
                Vector2 currentPosition = _Panel3_change.anchoredPosition;
                currentPosition.y = _postionOnSelectedY;
                _Panel3_change.anchoredPosition = currentPosition;
               
                //==================//
                ResetPositionOfPanel(_Panel1_change, -78.6f);
                ResetPositionOfPanel(_Panel2_change, -10.6f);
            }
        }
        
       
    }


    private void SetEquippedModel(GameObject selectedItem)  
    {
        if (_SelectedItemModel != null)
        {
            DestroyImmediate(_SelectedItemModel.gameObject);
            _SelectedItemModel = null;
        }

        string selectedItemName = selectedItem.name.Replace("(Clone)", "");
        _SelectedItemModel = Instantiate(Resources.Load<GameObject>(selectedItemName + "_Model"),
            new Vector3 (0.247f, 0.379f, 0.281f), Quaternion.Euler(10f, 90f, 15f));
        _SelectedItemModel.transform.SetParent(_toolHolder.transform, false);

    }

    GameObject getSelectedItem(int SlotNumber)
    {
        return quickSlotsList[SlotNumber - 1].transform.GetChild(0).gameObject;
    }




    bool checkPanelFull(int slotnumber)
    {
        if(quickSlotsList[slotnumber -1].transform.childCount > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void ResetPositionOfPanel(RectTransform panelTransform, float x)
    {
        Vector2 _defaultPosition = new Vector2(x, -183f);
        panelTransform.anchoredPosition = _defaultPosition;
        //_inventoryItem._isSelected = false;
    }



    private void PopulateSlotList()
    {
        foreach (Transform child in quickSlotsPanel.transform)
        {
            if (child.CompareTag("PanelSlot"))
            {
                quickSlotsList.Add(child.gameObject);
            }
        }
    }

    public void AddToQuickSlots(GameObject itemToEquip)
    {
        // пустой слот
        GameObject availableSlot = FindNextEmptySlot();
        // трансформ
        itemToEquip.transform.SetParent(availableSlot.transform, false);
        // получаем имя без клона 
        string cleanName = itemToEquip.name.Replace("(Clone)", "");
        itemList.Add(cleanName);

        InventoryManager.Instance.ReconstructLists();

    }


    private GameObject FindNextEmptySlot()
    {
        foreach (GameObject slot in quickSlotsList)
        {
            if (slot.transform.childCount == 0)
            {
                return slot;
            }
        }
        return new GameObject();
    }

    public bool CheckIfFull()
    {

        int counter = 0;

        foreach (GameObject slot in quickSlotsList)
        {
            if (slot.transform.childCount > 0)
            {
                counter += 1;
            }
        }

        if (counter == 7)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
