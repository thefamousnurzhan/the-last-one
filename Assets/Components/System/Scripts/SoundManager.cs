using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance { get; set; }

    public AudioSource _pickUpSound;

    //public AudioSource _running;
    public AudioSource _crafting;
    public AudioSource _axeSwing;
    public AudioSource _pistol;
    public AudioSource _sword;
    public AudioSource _walking;
    public AudioSource _zombieIdle;
    public AudioSource _zombiePunch;

    public AudioSource _environment;


    private void Awake()
    {
        if(Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public void PlaySound(AudioSource soundToPlay)
    {
        if (!soundToPlay.isPlaying)
        {
            soundToPlay.Play();
        }
    }

}
