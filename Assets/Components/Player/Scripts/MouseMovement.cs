using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMovement : MonoBehaviour
{
    public float mouseSensitivity = 800f;

    float xRotation = 0f;
    float YRotation = 0f;

    void Start()
    { 
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        if (!InventoryManager.Instance._isOpen && !CraftingManager.Instance._isOpen)
        {
            float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

            //вверх и вниз 
            xRotation -= mouseY;

            //как в реальной жизни
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);

            //вверх и вниз 
            YRotation += mouseX;

            //сразу две ротейтим
            transform.localRotation = Quaternion.Euler(xRotation, YRotation, 0f);
        }
        

    }   
}
