using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private CharacterController controller;

    [SerializeField] private float _speed = 12f;
    [SerializeField] private float _gravity = -9.81f * 2;
    [SerializeField] private float _jumpHeight = 3f;

    [SerializeField] private Transform _groundCheck;
    [SerializeField] private float _groundDistance = 0.4f;
    [SerializeField] private LayerMask _groundMask;

    private Vector3 _lastPosition = new Vector3(0f, 0f, 0f);
    public bool _isMoving;

    Vector3 velocity;

    bool _isGrounded;

    void Update()
    {
        _isGrounded = Physics.CheckSphere(_groundCheck.position, _groundDistance, _groundMask);

        if (_isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * _speed * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Space) && _isGrounded)
        {
            velocity.y = Mathf.Sqrt(_jumpHeight * -2f * _gravity);
        }


        velocity.y += _gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        if (_lastPosition != gameObject.transform.position && _isGrounded)
        {
            _isMoving = true;
            SoundManager.Instance.PlaySound(SoundManager.Instance._walking);
        }
        else
        {
            _isMoving = false;
            //SoundManager.Instance._walking.Stop();
        }
        _lastPosition = gameObject.transform.position;



    }
}
