using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public int _maxHealth = 100;
    private int _axeDamage = 45;
    //private int _swordDamage = 40;
    //private int _GunDamage = 50;
    public int _currentHealth;
    public PlayerHealth _playerHealth;
    public ZombieManager _zombieManager;
    //
    public float _Attackrange;
    public LayerMask _whatIsZombie;
    public bool _zombieIsInAttackRange;
    private void Start()
    {
        _currentHealth = _maxHealth;
        _playerHealth.SetMaxHealth(_maxHealth);
    }
    private void Update()
    {
        _zombieIsInAttackRange = Physics.CheckSphere(transform.position, _Attackrange, _whatIsZombie);

        if (_zombieIsInAttackRange && Input.GetMouseButtonDown(0))
        {
            AttackZombie();
        }
    }

    private void AttackZombie()
    {
        _zombieManager.TakeDamageToZombie(_axeDamage);
    }
    public void TakeDamageFromZombie(int damage) 
    {
        _currentHealth -= damage;
        _playerHealth.SetHealth(_currentHealth);
        if(_currentHealth <= 0)
        {
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene("Restart_Menu");
        }
    }
}
