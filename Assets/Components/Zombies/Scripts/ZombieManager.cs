using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class ZombieManager : MonoBehaviour
{
    public NavMeshAgent _agent;
    public Animator _animator;
    public Transform _player;
    public float _healthZombie;
    public int _damage;
    public LayerMask _whatIsGround, _whatIsPLayer;
    public PlayerController _playerController;
    //

    public Vector3 _walkPoint;
    private bool _walkPointSet;
    public float _walkPointRange;

    // Attacking
    public float _timeBetweenAttacks;
    bool _alreadyAttacked;
    //
    public float _sightRange, _attackRange;
    public bool _playerInSightRange, _playerInAttackRange;
    // playerController

    private void Awake()
    {
        _player = GameObject.Find("Player").transform;
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
    }


    private void Update()
    {
        _playerInSightRange = Physics.CheckSphere(transform.position, _sightRange, _whatIsPLayer);
        _playerInAttackRange = Physics.CheckSphere(transform.position, _attackRange, _whatIsPLayer);

        if(!_playerInSightRange && !_playerInAttackRange)
        {
            Patroling();
        }
        if(_playerInSightRange && !_playerInAttackRange)
        {
            SoundManager.Instance.PlaySound(SoundManager.Instance._zombieIdle);
            ChasePlayer();
        }
        else
        {
            SoundManager.Instance._zombieIdle.Stop();
        }
        if(_playerInAttackRange && _playerInSightRange)
        {
            SoundManager.Instance.PlaySound(SoundManager.Instance._zombiePunch);
            AttackPlayer();
        }
        else
        {
            SoundManager.Instance._zombiePunch.Stop();
        }
    }


    private void Patroling()
    {
        if (!_walkPointSet)
        {
            SearchWalkPoint();
        }

        if (_walkPointSet)
        {
            _agent.SetDestination(_walkPoint);
            _animator.SetBool("isWalking", true);
            
        }

        Vector3 _distanceToWalkPoint = transform.position - _walkPoint;

        if(_distanceToWalkPoint.magnitude < 1f)
        {
            _walkPointSet = false;
        }
    }
    private void ChasePlayer()
    {
        if (_agent.isOnNavMesh)
        {
            _agent.SetDestination(_player.position);
            _animator.SetBool("isWalking", true);
        }

    }
    private void AttackPlayer()
    {
            _agent.SetDestination(transform.position);
            transform.LookAt(_player);
            if (!_alreadyAttacked)
            {
                _playerController.TakeDamageFromZombie(_damage);
                _animator.SetBool("IsPunching", true);
                _alreadyAttacked = true;
                Invoke(nameof(ResetAttack), _timeBetweenAttacks);
            }
    }

    private void ResetAttack()
    {
        _alreadyAttacked = false;
    }

    private void SearchWalkPoint()
    {
        float randomZ = Random.Range(-_walkPointRange, _walkPointRange);
        float randomX = Random.Range(-_walkPointRange, _walkPointRange);

        _walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);
        if(Physics.Raycast(_walkPoint, -transform.up, 2f, _whatIsGround))
        {
            _walkPointSet = true;
        }
    }
    public void TakeDamageToZombie(int damage)
    {
        _healthZombie -= damage;
        if(_healthZombie <= 0)
        {
            _animator.SetBool("IsDying", true);

            StartCoroutine(Destroy());
            //Invoke(nameof(Destroy), 2f);
        }
    }
    private IEnumerator Destroy()
    {
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
        SoundManager.Instance._zombieIdle.Stop();
        SoundManager.Instance._zombiePunch.Stop();

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, _sightRange);
    }

}
